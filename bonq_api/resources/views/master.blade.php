<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{URL::asset('css/main.css')}}">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital@1&display=swap" rel="stylesheet">
    <title>Laravel CRUD</title>
  </head>
  <body>
    <div class="container">
      @yield('content')
    </div>
  </body>
</html>
