<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameStats extends Model
{
    protected $table = "game_stats";
}
