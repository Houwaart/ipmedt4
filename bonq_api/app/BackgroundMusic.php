<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BackgroundMusic extends Model
{
    protected $table = "background_music";
}
